# Markdown Experiments

Markdown experiments for linkedin training  
Add two spaces after sentences for new line

## Learning Goals
1. Have Fun
2. Learn gitlab
3. ...
4. Profit

## Colors

- `#F00`
`#F00`
- `RGB(00,255,0)`


## Diagrams and Flowcharts

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```

## Links

- This is my favorite website for stock market updates [CNBC](https://cnbc.com)
- [GitLab Markdown Guide](https://gitlab.com/gitlab-org/gitlab/blob/master/doc/user/markdown.md)

 **Bold section**
